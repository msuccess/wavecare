using System;
using System.Collections.Generic;
using System.Text;
using WaveCare.Domain;

namespace WaveCare.Data.Interfaces
{
	public interface IMedicationRepository: IRepository<Medication>
	{
		IEnumerable<Medication> GetMedications();

		Medication GetMedication(Guid id);
	}
}
