using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using WaveCare.Domain;

namespace WaveCare.Data.Interfaces
{
	public interface IAppointmentRepository: IRepository<Appointment>
	{

		Appointment GetAppointmentsById(Guid id);

		IEnumerable<Appointment> GetAppointments();
	}
}
