using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WaveCare.Domain;

namespace WaveCare.Data.Interfaces
{
    public interface IPatientRepository : IRepository<Patient>
    {
		Patient GetByIdWithRelatedAsync(Guid Id);

        IEnumerable<Patient> GetRecentlyAdded();

        IEnumerable<Patient> GetPatients();
	}
}
