﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WaveCare.Data.Repositories;
using WaveCare.Domain;

namespace WaveCare.Data.Interfaces
{
    public interface IDoctorRepository : IRepository<Doctor>
    {
        Doctor GetDoctorByEmail(string email);

        IEnumerable<Doctor> GetRecentlyAddedDectors();

        IEnumerable<Doctor> GetDoctors();

        IEnumerable<Doctor> GetDoctorByDuty();
    }
}