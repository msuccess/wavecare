﻿using System;
using System.Collections.Generic;
using System.Text;
using WaveCare.Domain;

namespace WaveCare.Data.Interfaces
{
    public interface INoteRepository
    {
        IEnumerable<Notes> GetAllForEntity(Guid entityid);
    }
}