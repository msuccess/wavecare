using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WaveCare.Domain;

namespace WaveCare.Data.Interfaces
{
    public interface IStaffRepository : IRepository<Staff>
    {
        Staff GetByIdWithRelated(Guid Id);

        IEnumerable<Staff> GetRecentlyAdded();

        IEnumerable<Staff> GetStaffs();
    }
}
