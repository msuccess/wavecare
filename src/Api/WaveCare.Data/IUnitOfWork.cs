using System;
using System.Collections.Generic;
using System.Text;
using WaveCare.Data.Interfaces;

namespace WaveCare.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IDoctorRepository Doctors { get; }

        IPatientRepository Patients { get; }

        INoteRepository Notes { get; }

        IStaffRepository Staffs { get; }

		IMedicineRespository Medicines { get; }

	    IMedicationRepository Medications{ get;}

		IAppointmentRepository Appointments { get; }
		int SaveChanges();
    }
}
