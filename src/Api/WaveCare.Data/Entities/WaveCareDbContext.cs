using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WaveCare.Domain;

namespace WaveCare.Data.Entities
{
	public class WaveCareDbContext : IdentityDbContext<AuthUser>
	{
		public WaveCareDbContext(DbContextOptions options)
			: base(options)
		{
		}

		public DbSet<Staff> Staffs { get; set; }
		public DbSet<Doctor> Doctors { get; set; }
		public DbSet<Notes> Notes { get; set; }
		public DbSet<Appointment> Appointments { get; set; }
		public DbSet<Medication> Medications { get; set; }
		public DbSet<Prescription> Prescriptions { get; set; }
		public DbSet<Patient> Patients { get; set; }
		public DbSet<EmergencyContacts> EmergencyContacts { get; set; }
		public DbSet<MedicalHistory> MedicalHistories { get; set; }
		public DbSet<Address> Address { get; set; }
	}
}
