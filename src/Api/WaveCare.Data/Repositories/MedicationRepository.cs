using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Domain;

namespace WaveCare.Data.Repositories
{
	public class MedicationRepository: Repository<Medication>, IMedicationRepository
	{
		public MedicationRepository(WaveCareDbContext dbContext) : base(dbContext)
		{
		}

		public IEnumerable<Medication> GetMedications()
		{
			var medication = DbSet
				.Include(a => a.Patient)
				.Include(a => a.Medicine)
				.ToList();

			return medication;

		}

		public Medication GetMedication(Guid id)
		{
			var medication = DbSet
				.Include(a => a.Patient)
				.Include(a => a.Medicine)
				.SingleOrDefault(a => a.Id == id);

			return medication;
		}
	}
}
