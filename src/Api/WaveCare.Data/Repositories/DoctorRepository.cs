using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Domain;

namespace WaveCare.Data.Repositories
{
	public class DoctorRepository : Repository<Doctor>, IDoctorRepository
	{
		public DoctorRepository(WaveCareDbContext context) : base(context)
		{ }

		public IEnumerable<Doctor> GetDoctorByDuty()
		{
			var doctors = DbSet
				.Include(a => a.Address)
				.Include(a => a.Appointments)
				.Where(a => a.DutyTpye == "Active")
				.ToList();

			return doctors;
		}

		public Doctor GetDoctorByEmail(string email)
		{
			var doctor = DbSet
				 .Include(a => a.Address)
				 .Include(a => a.Appointments)
				 .SingleOrDefault(a => a.EmailAddress == email);

			return doctor;
		}

		public IEnumerable<Doctor> GetDoctors()
		{
			var doctor = DbSet
				.Include(a => a.Address)
				.Include(a => a.Appointments)
				.ToList();

			return doctor;
		}

		public IEnumerable<Doctor> GetRecentlyAddedDectors()
		{
			var doctor = GetAll()
				.Where(a => a.Created > DateTime.Now.AddDays(-7));

			return doctor;
		}
	}
}
