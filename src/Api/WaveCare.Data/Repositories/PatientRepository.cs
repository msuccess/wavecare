using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Domain;

namespace WaveCare.Data.Repositories
{
	public class PatientRepository : Repository<Patient>, IPatientRepository
	{
		public PatientRepository(WaveCareDbContext context) : base(context)
		{
		}

		public Patient GetByIdWithRelatedAsync(Guid Id)
		{
			var patient = DbSet
				.Include(a => a.Address)
				.Include(a => a.Appointments)
				.Include(a => a.EmergencyContacts)
				.Include(a => a.MedicalHistorys)
				.SingleOrDefault(a => a.Id == Id);

			return patient;
		}

		public IEnumerable<Patient> GetRecentlyAdded()
		{
			var patients = GetAll()
				.Where(a => a.Created > DateTime.Now.AddDays(-7));
			return patients;
		}

		public IEnumerable<Patient> GetPatients()
		{
			var patients = DbSet
				.Include(a => a.Address)
				.Include(a => a.Appointments)
				.Include(a => a.EmergencyContacts)
				.Include(a => a.MedicalHistorys)
				.ToList();

			return patients;
		}
	}
}
