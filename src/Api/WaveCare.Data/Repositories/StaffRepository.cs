using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Domain;

namespace WaveCare.Data.Repositories
{
    public class StaffRepository : Repository<Staff>, IStaffRepository
    {
        public StaffRepository(WaveCareDbContext context) : base(context)
        {
        }

		public Staff GetByIdWithRelated(Guid Id)
        {
            var staff = DbSet

                .Include(a => a.EmergencyContacts)

                .SingleOrDefault(a => a.Id == Id);

            return staff;
        }

        public IEnumerable<Staff> GetRecentlyAdded()
        {
            var staff = GetAll()

                .Where(a => a.Created > DateTime.Now.AddDays(-7));

            return staff;
        }

		public IEnumerable<Staff> GetStaffs()
		{
			var staffs = DbSet
				.Include(a => a.EmergencyContacts)
				.ToList();

			return  staffs;
		}
	}
}
