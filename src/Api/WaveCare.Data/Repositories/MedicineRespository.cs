using System;
using System.Collections.Generic;
using System.Text;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Domain;

namespace WaveCare.Data.Repositories
{
	public class MedicineRespository: Repository<Medicine>, IMedicineRespository
	{
		public MedicineRespository(WaveCareDbContext dbContext) : base(dbContext)
		{
		}

	}
}
