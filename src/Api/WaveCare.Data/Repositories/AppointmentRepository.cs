using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Domain;

namespace WaveCare.Data.Repositories
{
	public class AppointmentRepository : Repository<Appointment>, IAppointmentRepository
	{
		public AppointmentRepository(WaveCareDbContext dbContext) : base(dbContext)
		{
		}

		public Appointment GetAppointmentsById(Guid id)
		{
			var appointment = DbSet
				.Include(a => a.Patient)
				.Include(a => a.Patient)
				.SingleOrDefault(a => a.Id == id);

			return appointment;
		}

		public IEnumerable<Appointment> GetAppointments()
		{
			var appointment = DbSet

				.Include(a => a.Patient)
				.Include(a => a.Doctor)
				.ToList();

			return appointment;
		}
	}
}
