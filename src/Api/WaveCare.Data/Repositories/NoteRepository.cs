﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Domain;

namespace WaveCare.Data.Repositories
{
	public class NoteRepository : Repository<Notes>, INoteRepository
	{
		public NoteRepository(WaveCareDbContext context) : base(context)
		{
		}

		public IEnumerable<Notes> GetAllForEntity(Guid entityid)
		{
			var notes = DbSet
				.Where(a => a.ParentId == entityid).ToList();

			return notes;
		}
	}
}