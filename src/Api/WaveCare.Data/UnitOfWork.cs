using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Data.Repositories;

namespace WaveCare.Data
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly WaveCareDbContext dbContext;
		private readonly IMedicineRespository _medicines;

		public UnitOfWork(
			WaveCareDbContext _dbContext,
			IMedicationRepository medications,
			IAppointmentRepository appointment,
			IMedicineRespository medicines
			)
		{
			dbContext = _dbContext;
			_medicines = medicines;

			Medicines = new MedicineRespository(_dbContext);

			Medications = new MedicationRepository(_dbContext);

			Appointments = new AppointmentRepository(_dbContext);

			Doctors = new DoctorRepository(_dbContext);

			Patients = new PatientRepository(_dbContext);

			Notes = new NoteRepository(_dbContext);

			Staffs = new StaffRepository(_dbContext);
		}

		public IDoctorRepository Doctors { get; }

		public IPatientRepository Patients { get; }

		public INoteRepository Notes { get; }

		public IStaffRepository Staffs { get; }

		public IMedicationRepository Medications { get; }

		public IAppointmentRepository Appointments { get; }

		public IMedicineRespository Medicines { get; }

		public void Dispose()
		{
			dbContext.Dispose();
		}

		public int SaveChanges()
		{
			return dbContext.SaveChanges();
		}
	}
}
