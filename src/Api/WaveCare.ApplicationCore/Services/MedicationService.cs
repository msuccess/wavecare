using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Data;
using WaveCare.Domain;
using WaveCare.Infrastructure.Constants;
using WaveCare.Infrastructure.Results;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Services
{
	public class MedicationService : ServiceBase, IMedicationService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;

		public MedicationService(IUnitOfWork uow, IMapper mapper)
		{
			_uow = uow;
			_mapper = mapper;
		}

		public IResult AddMedication(Medication medication)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				_uow.Medications.Add(medication);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.AddOperationCompletedSuccessfully);
			});
		}

		public IResult DeleteMedication(Guid id)
		{
			if (string.IsNullOrEmpty(id.ToString()))
				return new Result(ResultType.Error, MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult>(() =>
			{
				var medication = _uow.Medications.GetById(id);

				if (medication == null)

					return new Result(ResultType.Error, MessageSource.NoDataFound);

				_uow.Medications.Remove(medication);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}

		public IResult<Medication> GetMedicationById(Guid id)
		{
			if (id == null)

				throw new ArgumentNullException(MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult<Medication>>(() =>
			{
				var medication = _uow.Medications.GetMedication(id);

				return new Result<Medication>(medication, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult<IEnumerable<Medication>> GetMedications()
		{
			return ExecuteHandledOperation<IResult<IEnumerable<Medication>>>(() =>
			{
				var medication = _uow.Medications.GetMedications()
								 .ToList();

				//doctor = doctor.ApplySorting(queryObject).ApplyPaging(queryObject);

				return new Result<IEnumerable<Medication>>(medication, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult UpdateMedication(Guid Id, Medication medication)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				var result = _uow.Doctors.GetById(Id);

				if (result == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				//				medication.CopyProperties(medication);

				medication.LastUpdated = DateTime.Now;

				_uow.Medications.Update(medication);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}
	}
}
