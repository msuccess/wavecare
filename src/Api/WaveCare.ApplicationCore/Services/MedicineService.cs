using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Data;
using WaveCare.Domain;
using WaveCare.Infrastructure.Constants;
using WaveCare.Infrastructure.Extentions;
using WaveCare.Infrastructure.Results;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Services
{
	public class MedicineService : ServiceBase, IMedicineService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;

		public MedicineService(IUnitOfWork uow, IMapper mapper)
		{
			_uow = uow;
			_mapper = mapper;
		}

		public IResult AddMedicine(Medicine medicine)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				_uow.Medicines.Add(medicine);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.AddOperationCompletedSuccessfully);
			});
		}

		public IResult DeleteMedicine(Guid id)
		{
			if (string.IsNullOrEmpty(id.ToString()))
				return new Result(ResultType.Error, MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult>(() =>
			{
				var medication = _uow.Medications.GetById(id);

				if (medication == null)

					return new Result(ResultType.Error, MessageSource.NoDataFound);

				_uow.Medications.Remove(medication);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}

		public IResult<Medicine> GetMedicineById(Guid id)
		{
			if (id == null)

				throw new ArgumentNullException(MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult<Medicine>>(() =>
			{
				var medicine = _uow.Medicines.GetById(id);

				return new Result<Medicine>(medicine, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult<IEnumerable<Medicine>> GetMedicines()
		{
			return ExecuteHandledOperation<IResult<IEnumerable<Medicine>>>(() =>
			{
				var medicine = _uow.Medicines.GetAll()
									.ToList();

				//doctor = doctor.ApplySorting(queryObject).ApplyPaging(queryObject);

				return new Result<IEnumerable<Medicine>>(medicine, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult UpdateMedicine(Guid id, Medicine medicine)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				var result = _uow.Medicines.GetById(id);

				if (result == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				medicine.CopyProperties(medicine);

				medicine.LastUpdated = DateTime.Now;

				_uow.Medicines.Update(medicine);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}
	}
}
