using AutoMapper;
using System;
using System.Collections.Generic;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Data;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Services
{
	public class NoteService : ServiceBase, INoteService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;

		public NoteService(IUnitOfWork uow, IMapper mapper)
		{
			_uow = uow;
			_mapper = mapper;
		}

		public IResult<IEnumerable<Notes>> GetNotes(Guid id)
		{
			throw new NotImplementedException();
		}
	}
}
