using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Data;
using WaveCare.Domain;
using WaveCare.Infrastructure.Constants;
using WaveCare.Infrastructure.Results;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Services
{
	public class DoctorService : ServiceBase, IDoctorService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;
		private readonly UserManager<AuthUser> _userManager;
		private readonly string _userId;

		public DoctorService(
			IUnitOfWork uow,
			IMapper mapper,
			UserManager<AuthUser> userManager,
			IHttpContextAccessor contextAccessor)
		{
			_uow = uow;
			_mapper = mapper;
			_userManager = userManager;
			_userId = contextAccessor.HttpContext.User.FindFirst("Id")?.Value;
		}

		public IResult AddDoctor(Doctor doctor)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				doctor.LastUpdated = doctor.Created;

				//	doctor.CreatedBy = _userManager.FindByIdAsync(_userId);

				_uow.Doctors.Add(doctor);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.AddOperationCompletedSuccessfully);
			});
		}

		public IResult DeleteDoctor(Guid Id)
		{
			if (string.IsNullOrEmpty(Id.ToString()))
				return new Result(ResultType.Error, MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult>(() =>
			{
				var doctor = _uow.Doctors.GetById(Id);

				if (doctor == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				_uow.Doctors.Remove(doctor);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}

		public IResult<Doctor> GetDoctorByEmail(string email)
		{
			if (string.IsNullOrEmpty(email))

				throw new ArgumentNullException(MessageSource.NullValueProvided);

			return ExecuteHandledOperation<IResult<Doctor>>(() =>
			{
				var doctor = _uow.Doctors.GetDoctorByEmail(email);

				return new Result<Doctor>(doctor, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult<Doctor> GetDoctorById(Guid Id)
		{
			if (Id == null)

				throw new ArgumentNullException(MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult<Doctor>>(() =>
			{
				var doctor = _uow.Doctors.GetById(Id);

				return new Result<Doctor>(doctor, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult<IEnumerable<Doctor>> GetDoctors(bool includeNavigationProperties = true)
		{
			return ExecuteHandledOperation<IResult<IEnumerable<Doctor>>>(() =>
			{
				var doctor = includeNavigationProperties
							 ? _uow.Doctors.GetDoctors()
							 : _uow.Doctors.GetAll().ToList();

				//doctor = doctor.ApplySorting(queryObject).ApplyPaging(queryObject);

				return new Result<IEnumerable<Doctor>>(doctor, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult UpdateDoctor(Guid Id, Doctor updateDoctor)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				var result = _uow.Doctors.GetById(Id);

				if (result == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				//updateDoctor.CreatedBy = _userManager.FindByIdAsync(_userId);

				updateDoctor.LastUpdated = DateTime.Now;

				_uow.Doctors.Update(updateDoctor);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}
	}
}
