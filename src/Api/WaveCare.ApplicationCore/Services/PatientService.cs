using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Data;
using WaveCare.Domain;
using WaveCare.Infrastructure.Constants;
using WaveCare.Infrastructure.Results;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Services
{
	public class PatientService : ServiceBase, IPatientService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;

		public PatientService(IUnitOfWork uow, IMapper mapper)
		{
			_uow = uow;
			_mapper = mapper;
		}

		public IResult<IEnumerable<Patient>> GetPatients(bool includeNavigationProperties = true)
		{
			return ExecuteHandledOperation<IResult<IEnumerable<Patient>>>(() =>
			{
				var patient = includeNavigationProperties ?
					_uow.Patients.GetPatients() :
					_uow.Patients.GetAll().ToList();

				//patient = patient.ApplySorting(queryObject).ApplyPaging(queryObject);

				return new Result<IEnumerable<Patient>>(patient, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult<Patient> GetPatientById(Guid Id)
		{
			if (Id == null)

				throw new ArgumentNullException(MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult<Patient>>(() =>
			{
				var patient = _uow.Patients.GetById(Id);

				return new Result<Patient>(patient, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult AddPatient(Patient patient)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				_uow.Patients.Add(patient);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.AddOperationCompletedSuccessfully);
			});
		}

		public IResult UpdatePatient(Guid Id, Patient updatePatient)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				var result = _uow.Doctors.GetById(Id);

				if (result == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				//doctor.CopyProperties(updatePatient);

				updatePatient.LastUpdated = DateTime.Now;

				_uow.Patients.Update(updatePatient);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}

		public IResult DeletePatient(Guid Id)
		{
			if (string.IsNullOrEmpty(Id.ToString()))
				return new Result(ResultType.Error, MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult>(() =>
			{
				var patient = _uow.Patients.GetById(Id);

				if (patient == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				_uow.Patients.Remove(patient);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}
	}
}
