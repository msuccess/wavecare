using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Data;
using WaveCare.Domain;
using WaveCare.Infrastructure.Constants;
using WaveCare.Infrastructure.Results;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Services
{
	public class AppointmentService : ServiceBase, IAppointmentService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;

		public AppointmentService(IUnitOfWork uow, IMapper mapper)
		{
			_uow = uow;
			_mapper = mapper;
		}

		public IResult AddAppointment(Appointment appointment)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				_uow.Appointments.Add(appointment);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.AddOperationCompletedSuccessfully);
			});
		}

		public IResult DeleteAppointment(Guid id)
		{
			if (string.IsNullOrEmpty(id.ToString()))
				return new Result(ResultType.Error, MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult>(() =>
			{
				var appointment = _uow.Appointments.GetById(id);

				if (appointment == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				_uow.Appointments.Remove(appointment);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}

		public IResult<Appointment> GetAppointmentById(Guid id)
		{
			if (id == null)

				throw new ArgumentNullException(MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult<Appointment>>(() =>
			{
				var appointment = _uow.Appointments.GetById(id);

				return new Result<Appointment>(appointment, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult<IEnumerable<Appointment>> GetAppointments()
		{
			return ExecuteHandledOperation<IResult<IEnumerable<Appointment>>>(() =>
			{
				var appointment = _uow.Appointments.GetAll().ToList();

				//doctor = doctor.ApplySorting(queryObject).ApplyPaging(queryObject);

				return new Result<IEnumerable<Appointment>>(appointment, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult UpdateAppointment(Guid id, Appointment appointment)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				var result = _uow.Appointments.GetById(id);

				if (result == null)

					return new Result(ResultType.Error, MessageSource.NoDataFound);

				//				appointment.CopyProperties(appointmentDto);

				appointment.LastUpdated = DateTime.Now;

				_uow.Appointments.Update(appointment);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}
	}
}
