using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Data;
using WaveCare.Domain;
using WaveCare.Infrastructure.Constants;
using WaveCare.Infrastructure.Results;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Services
{
	public class StaffService : ServiceBase, IStaffService
	{
		private readonly IUnitOfWork _uow;

		public StaffService(IMapper mapper, IUnitOfWork uow)
		{
			_uow = uow;
		}

		public IResult AddStaff(Staff staff)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				_uow.Staffs.Add(staff);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.AddOperationCompletedSuccessfully);
			});
		}

		public IResult DeleteStaff(Guid id)
		{
			if (string.IsNullOrEmpty(id.ToString()))

				return new Result(ResultType.Error, MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult>(() =>
			{
				var staff = _uow.Staffs.GetById(id);

				if (staff == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				_uow.Staffs.Remove(staff);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.DeleteOperationCompletedSuccessfully);
			});
		}

		public IResult<Staff> GetStaffById(Guid id)
		{
			if (id == null)

				throw new ArgumentNullException(MessageSource.NullIdProvided);

			return ExecuteHandledOperation<IResult<Staff>>(() =>
			{
				var staff = _uow.Staffs.GetByIdWithRelated(id);

				return new Result<Staff>(staff, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult<IEnumerable<Staff>> GetStaffs(bool includeNavigationProperties = true)
		{
			return ExecuteHandledOperation<IResult<IEnumerable<Staff>>>(() =>
			{
				var staff = includeNavigationProperties ?
					_uow.Staffs.GetStaffs() :
					_uow.Staffs.GetAll().ToList();

				return new Result<IEnumerable<Staff>>(staff, MessageSource.FetchOperationCompletedSuccessfully);
			});
		}

		public IResult UpdateStaff(Guid id, Staff updateStaff)
		{
			return ExecuteHandledOperation<IResult>(() =>
			{
				var result = _uow.Staffs.GetById(id);

				if (result == null) return new Result(ResultType.Error, MessageSource.NoDataFound);

				//updateStaff.CopyProperties(updateStaff);

				updateStaff.LastUpdated = DateTime.Now;

				_uow.Staffs.Update(updateStaff);

				_uow.SaveChanges();

				return new Result(ResultType.Success, MessageSource.UpdateOperationCompletedSuccessfully);
			});
		}
	}
}
