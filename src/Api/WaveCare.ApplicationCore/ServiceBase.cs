using System;

namespace WaveCare.ApplicationCore
{
	public class ServiceBase
	{
		protected T ExecuteHandledOperation<T>(Func<T> codetoExecute)
		{
			try
			{
				return codetoExecute.Invoke();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		protected void ExecuteHandledOperation(Action codetoExecute)
		{
			try
			{
				codetoExecute.Invoke();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
