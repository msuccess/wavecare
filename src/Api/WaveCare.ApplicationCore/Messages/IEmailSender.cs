using System.Threading.Tasks;

namespace WaveCare.ApplicationCore.Messages
{
	public interface IEmailSender
	{
		Task SendEmailAsync(string email, string subject, string message);
	}
}
