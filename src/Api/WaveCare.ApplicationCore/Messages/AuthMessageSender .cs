using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using WaveCare.Domain;

namespace WaveCare.ApplicationCore.Messages
{
	public class AuthMessageSender : IEmailSender
	{
		public AuthMessageSender(IOptions<EmailSettings> emailSettings)
		{
			_emailSettings = emailSettings.Value;
		}

		public EmailSettings _emailSettings { get; }

		public Task SendEmailAsync(string email, string subject, string message)
		{
			Execute(email, subject, message).Wait();
			return Task.FromResult(0);
		}

		public async Task Execute(string email, string subject, string message)
		{
			try
			{
				var toEmail = string.IsNullOrEmpty(email)
					? _emailSettings.ToEmail
					: email;

				var mail = new MailMessage()
				{
					From = new MailAddress(_emailSettings.UsernameEmail, "WaveCare")
				};

				mail.To.Add(new MailAddress(toEmail));
				mail.CC.Add(new MailAddress(_emailSettings.CcEmail));

				mail.Subject = "WaveCare Management System - " + subject;
				mail.Body = message;
				mail.IsBodyHtml = true;
				mail.Priority = MailPriority.High;

				var smtp = new SmtpClient(_emailSettings.SecondayDomain, _emailSettings.SecondaryPort);

				using (smtp)
				{
					smtp.Credentials = new NetworkCredential(_emailSettings.UsernameEmail, _emailSettings.UsernamePassword);
					smtp.EnableSsl = true;
					await smtp.SendMailAsync(mail);
				}
			}
			catch (Exception exception)
			{
				var ex = exception;
				return;
			}
		}
	}
}
