using System;
using System.Collections.Generic;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Interfaces
{
	public interface IMedicineService
	{
		IResult<IEnumerable<Medicine>> GetMedicines();

		IResult<Medicine> GetMedicineById(Guid id);

		IResult AddMedicine(Medicine medicine);

		IResult UpdateMedicine(Guid id, Medicine medicineDto);

		IResult DeleteMedicine(Guid id);
	}
}
