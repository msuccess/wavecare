using System;
using System.Collections.Generic;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Interfaces
{
	public interface IStaffService
	{
		IResult<IEnumerable<Staff>> GetStaffs(bool includeNavigationProperties = true);

		IResult<Staff> GetStaffById(Guid id);

		IResult AddStaff(Staff staff);

		IResult UpdateStaff(Guid id, Staff staff);

		IResult DeleteStaff(Guid id);
	}
}
