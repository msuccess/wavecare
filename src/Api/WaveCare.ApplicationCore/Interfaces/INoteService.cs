using System;
using System.Collections.Generic;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Interfaces
{
	public interface INoteService
	{
		IResult<IEnumerable<Notes>> GetNotes(Guid id);

		//		IResult<Notes> GetNoteUser(UserBase userBase);
		//
		//		IResult AddNote(Note note);
		//
		//		IResult UpdateNote(Guid Id, Note updateNote);
		//
		//		IResult DeleteNote(Guid Id);
	}
}
