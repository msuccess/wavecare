using System;
using System.Collections.Generic;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Interfaces
{
	public interface IPatientService
	{
		IResult<IEnumerable<Patient>> GetPatients(bool includeNavigationProperties = true);

		IResult<Patient> GetPatientById(Guid Id);

		IResult AddPatient(Patient patient);

		IResult UpdatePatient(Guid Id, Patient updatePatient);

		IResult DeletePatient(Guid Id);
	}
}
