using System;
using System.Collections.Generic;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Interfaces
{
	public interface IAppointmentService
	{
		IResult<IEnumerable<Appointment>> GetAppointments();

		IResult<Appointment> GetAppointmentById(Guid Id);

		IResult AddAppointment(Appointment doctor);

		IResult UpdateAppointment(Guid Id, Appointment appointmentDto);

		IResult DeleteAppointment(Guid Id);
	}
}
