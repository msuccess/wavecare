using System;
using System.Collections.Generic;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Interfaces
{
	public interface IMedicationService
	{
		IResult<IEnumerable<Medication>> GetMedications();

		IResult<Medication> GetMedicationById(Guid Id);

		IResult AddMedication(Medication medication);

		IResult UpdateMedication(Guid Id, Medication medicationDto);

		IResult DeleteMedication(Guid Id);
	}
}
