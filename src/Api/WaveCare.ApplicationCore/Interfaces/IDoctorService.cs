using System;
using System.Collections.Generic;
using WaveCare.Domain;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.ApplicationCore.Interfaces
{
	public interface IDoctorService
	{
		IResult<IEnumerable<Doctor>> GetDoctors(bool includeNavigationProperties = true);

		IResult<Doctor> GetDoctorById(Guid Id);

		IResult<Doctor> GetDoctorByEmail(string email);

		IResult AddDoctor(Doctor doctor);

		IResult UpdateDoctor(Guid Id, Doctor updateDoctor);

		IResult DeleteDoctor(Guid Id);
	}
}
