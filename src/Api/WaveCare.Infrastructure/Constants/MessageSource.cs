using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Infrastructure.Constants
{
    public class MessageSource
    {
        public const string NoResults = "Could Not Retrive Results";

        public const string OperationFailed = "Operation Failed";

        public const string ValueAlreadyExist = "Value Already Exist";

        public const string NoTypeMatch = "No Type Match";

        public const string NoDataFound = "No Data Found";

        public const string NullValueProvided = "Null Value Provided";

        public const string UpdateOperationCompletedSuccessfully = "Update Operation Completed Successfully";

        public const string AddOperationCompletedSuccessfully = "Add Operation Completed Successfully";

        public const string DeleteOperationCompletedSuccessfully = "Delete Operation Completed Successfully";

        public const string FetchOperationCompletedSuccessfully = "Get Operation Completed Successfully";

        public const string NullIdProvided = "Empty Id provided";

        public static string CannotBeNull(string name) => $"Parameter {name} cannot be null";
    }
}
