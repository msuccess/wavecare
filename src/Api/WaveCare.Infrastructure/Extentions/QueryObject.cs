namespace WaveCare.Infrastructure.Extentions
{
	public class QueryObject
	{
		public int PageSize { get; set; }
		public int Page { get; set; }
		public string OrderBy { get; set; } = "";
		public string SortBy { get; set; } = "";
	}
}
