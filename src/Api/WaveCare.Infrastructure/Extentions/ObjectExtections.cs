namespace WaveCare.Infrastructure.Extentions
{
	public static class ObjectExtections
	{
		public static void CopyProperties(this object self, object parent)
		{
			var fromProperties = parent.GetType().GetProperties();
			var toProperties = self.GetType().GetProperties();

			foreach (var fromProperty in fromProperties)
			{
				foreach (var toProperty in toProperties)
				{
					if (fromProperty.Name == toProperty.Name && fromProperty.PropertyType == toProperty.PropertyType)
					{
						toProperty.SetValue(self, fromProperty.GetValue(parent));
						break;
					}
				}
			}
		}
	}
}
