using System;
using System.Collections.Generic;
using System.Linq;

namespace WaveCare.Infrastructure.Extentions
{
	public static class QuerableExtensions
	{
		public static IEnumerable<T> ApplyPaging<T>(this IEnumerable<T> query, QueryObject queryObject)
		{
			if (queryObject == null) queryObject = new QueryObject();

			if (queryObject.Page == 0) queryObject.Page = 1;

			if (queryObject.PageSize == 0) queryObject.PageSize = 10;

			var data = query.Skip((queryObject.Page - 1) * queryObject.PageSize);

			return data.Take(queryObject.PageSize).ToList();
		}

		public static IEnumerable<T> ApplySorting<T>(this IEnumerable<T> query, QueryObject queryObject)
		{
			if (query == null)

				throw new ArgumentException("query");

			if (queryObject == null)

				throw new ArgumentException("queryObject");

			return query.OrderBy(a => a.GetType()
									   .GetProperty(queryObject.SortBy))
									   .ToList();
		}
	}
}
