using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Infrastructure.Results.Interface
{
    public interface IResult
    {
        bool IsSucceeded { get; }
        ResultType Type { get; }
        string Message { get; }
    }
}
