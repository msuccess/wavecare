using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Infrastructure.Results.Interface
{
    public interface IResult<T> : IResult
    {
		T Data { get; }
	    bool HasData { get; }

	    IResult<T> Set(T data, string message = null);

	    void Clear();

	}
}
