using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Infrastructure.Results
{
    public class GenericTypeHelper<T>
    {
        public static bool GetHasValue(T source)
        {
            if (typeof(T).IsValueType) return true;
            if (Equals(source, null)) return false;
            var enumerable = (IEnumerable)source;
            return enumerable.GetEnumerator().MoveNext();
        }
    }
}
