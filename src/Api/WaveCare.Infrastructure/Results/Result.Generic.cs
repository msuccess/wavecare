using System;
using System.Collections.Generic;
using System.Text;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.Infrastructure.Results
{
    public class Result<TResult> : Result, IResult<TResult>
    {
		
	    public Result(ResultType type, string message)
	    {
		    Type = type;
		    IsSucceeded = Type == ResultType.Success;
		    Message = message;
	    }

	    public Result(TResult data) => Set(data);

	    public Result(TResult data, string message) => Set(data, message);

	    public TResult Data { get; private set; }

	    public bool HasData => IsSucceeded && GenericTypeHelper<TResult>.GetHasValue(Data);

	    public IResult<TResult> Set(TResult data, string message = null)
	    {
		    Data = data;
		    IsSucceeded = true;
		    if (!string.IsNullOrWhiteSpace(message))
			    Message = message;
		    return this;
	    }

	    public void Clear()
	    {
		    IsSucceeded = false;
		    Data = default(TResult);
	    }


	}
}
