using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveCare.Infrastructure.Results.Interface;

namespace WaveCare.Infrastructure.Results
{
    public class Result : IResult
    {
        [DataMember]
        public static readonly IResult EmptySucceeded = new Result(ResultType.Success, "Success");

        protected Result()
        {
        }

        public Result(ResultType type, string message)
        {
            Type = type;
            IsSucceeded = Type == ResultType.Success;
            Message = message;
        }

        [DataMember]
        public bool IsSucceeded { get; protected set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public ResultType Type { get; set; }
    }
}
