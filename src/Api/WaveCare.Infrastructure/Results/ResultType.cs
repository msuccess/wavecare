using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Infrastructure.Results
{
    public enum ResultType
    {
        Unknown = 0,
        Success = 1,
        Warning = 2,
        Error = 3,
        Critical = 4
    }
}
