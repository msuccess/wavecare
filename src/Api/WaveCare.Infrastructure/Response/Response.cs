using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace WaveCare.Infrastructure.Response
{
	public class Response
	{
		public int StatusCode { get; set; }

		public Response(int statusCode)
		{
			StatusCode = statusCode;
			Message = GetDefaultMessageForStatusCode(statusCode);
		}

		public Response(int statusCode, string message)
		{
			StatusCode = statusCode;
			Message = message;
		}

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string Message { get; set; }

		private static string GetDefaultMessageForStatusCode(int statusCode)
		{
			switch (statusCode)
			{
				case 200: return "Operation successful";
				case 201: return "Resource created successful";
				case 404: return "Resource not found";
				case 500: return "An unhandled error occurred";
				default: return null;
			}
		}
	}

}
