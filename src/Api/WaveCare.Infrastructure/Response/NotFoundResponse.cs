using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Infrastructure.Response
{
	public class NotFoundResponse:Response
	{
		public NotFoundResponse() : base(404)
		{
		}

		public NotFoundResponse(string message) : base(404)
		{
			Message = message;
		}

	}
}
