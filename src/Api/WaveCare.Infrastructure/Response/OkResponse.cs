using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaveCare.Infrastructure.Response
{
	public class OkResponse : Response
	{
		public object Data { get; set; }

		public OkResponse(object data) : base(200) => Data = data;

		public OkResponse(string message, object data) : base(200)
		{
			Data = data;
			Message = message;
		}
	}

	public class OkListResponse : Response
	{
		public int TotalCount { get; set; }
		public IEnumerable<object> Data { get; set; }

		public OkListResponse(IEnumerable<object> data) : base(200)
		{
			Data = data;
			TotalCount = data.Count();
		}

		public OkListResponse(string message, IEnumerable<object> data) : base(200)
		{
			Data = data;
			Message = message;
			TotalCount = data.Count();
		}
	}

}
