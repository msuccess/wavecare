using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WaveCare.Infrastructure.Response
{
	public class BadRequestResponse : Response
	{
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public ICollection<string> Errors { get; set; }

		public BadRequestResponse(string message) : base(400)
		{
			Message = message;
		}

		public BadRequestResponse(ModelStateDictionary modelState) : base(400)
		{
			if (modelState.IsValid)
				throw new ArgumentException("ModelState must be invalid", nameof(modelState));

			Errors = modelState
				.SelectMany(x => x.Value.Errors)
				.Select(x => x.ErrorMessage)
				.ToArray();
		}
	}
}
