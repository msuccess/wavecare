using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Domain
{

    public class MedicalHistory: Entity
    {
        public string Description { get; set; }
        public ICollection<Prescription> Prescription { get; set; }
        public Patient Patient { get; set; }
    }
}
