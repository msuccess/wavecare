using System;
using System.Collections.Generic;

namespace WaveCare.Domain
{
	public class Patient : UserBase
	{
		public string InsuranceNumber { get; set; }
		public string Referrer { get; set; }
		public string ReferenceNumber { get; set; } //TODO:Create how to get reference number
		public DateTime RegistrationDate { get; set; }
		public DateTime ReleaseDate { get; set; }
		public string PatientStatus { get; set; }//under treatment or cured
		public string PatientType { get; set; }// inpatient or Outpatient

		//More Details
		public string SpouseName { get; set; }

		public string MaidenName { get; set; }
		public string PlaceOfBirth { get; set; }
		public string Religion { get; set; }
		public string Company { get; set; }

		public ICollection<EmergencyContacts> EmergencyContacts { get; set; }

		//Lists
		public ICollection<MedicalHistory> MedicalHistorys { get; set; }

		public Notes Note { get; set; }

		public ICollection<Appointment> Appointments { get; set; }
		public Doctor Doctor { get; set; }

		//Billings
	}
}
