using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Domain
{
    public class Medication:Entity
    {
        public double Cost { get; set; }
        public Patient Patient { get; set; }
        public string Treatment { get; set; }
        public ICollection<Medicine> Medicine { get; set; }

    }
}
