using Microsoft.AspNetCore.Identity;

namespace WaveCare.Domain
{
	public class AuthUser : IdentityUser
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }
	}
}
