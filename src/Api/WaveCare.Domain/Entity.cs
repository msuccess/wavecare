using System;
using System.ComponentModel.DataAnnotations;

namespace WaveCare.Domain
{
	public class Entity
	{
		[Key]
		public Guid Id { get; set; } = Guid.NewGuid();

		//public Task<AuthUser> CreatedBy { get; set; }

		public DateTime? LastUpdated { get; set; }
		public AuthUser LastUpdatedBy { get; set; }
		public DateTime? LastViewed { get; set; }
		public DateTime Created { get; set; } = DateTime.Now;
	}
}
