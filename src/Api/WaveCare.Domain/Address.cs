using System;
using System.ComponentModel.DataAnnotations;

namespace WaveCare.Domain
{
	public class Address 
	{
		[Key]
		public Guid Id { get; set; } = Guid.NewGuid();

		public string Street { get; set; }
		public string LandMark { get; set; }
		public string City { get; set; }
		public string Region { get; set; }
		public string Country { get; set; }
		public string ZipCode { get; set; }
		public string State { get; set; }
		public string PostalCode { get; set; }
	}
}
