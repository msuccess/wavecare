using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Domain
{
    public class Appointment : Entity
    {
        public DateTime AppointmentDate { get; set; }
        public string TreatmentFor { get; set; }
        public string Specialization { get; set; }
        public string Status { get; set; }
        public string Details { get; set; }
        public Doctor Doctor { get; set; }
        public Patient Patient { get; set; }
    }
}
