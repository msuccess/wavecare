using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Domain
{
    public class Notes : Entity
    {
        public string Name { get; set; }
        public string Details { get; set; }
        public Guid ParentId { get; set; }
    }
}
