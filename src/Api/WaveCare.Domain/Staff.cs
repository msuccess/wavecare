using System;
using System.Collections.Generic;

namespace WaveCare.Domain
{
	public class Staff : UserBase
	{
		public string Department { get; set; }
		public DateTime DateEmployed { get; set; }
		public DateTime RetirementDate { get; set; }
		public ICollection<EmergencyContacts> EmergencyContacts { get; set; }
		public string EmployeeType { get; set; }
		public string Qualification { get; set; }
		public string Specialization { get; set; }
	}
}
