using System;

namespace WaveCare.Domain
{
	public class UserBase : Entity
	{
		// User Details
		public string Title { get; set; }

		public string UploadImage { get; set; }
		public string IdentityId { get; set; } = "";
		public AuthUser Identity { get; set; } = new AuthUser();
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string EmailAddress { get; set; }
		public string Password { get; set; }
		public string PhoneNumber { get; set; }
		public string MobileNumber { get; set; }
		public string BusinessPhone { get; set; }
		public string Occupation { get; set; }
		public string IsActive { get; set; }
		public string MaritalStatus { get; set; }
		public string Gender { get; set; }
		public DateTime DateOfBirth { get; set; }
		public string BackgroundInfo { get; set; }
		public string BloodGroup { get; set; }

		public Address Address { get; set; }
	}
}
