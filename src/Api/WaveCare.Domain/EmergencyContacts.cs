using System;
using System.Collections.Generic;
using System.Text;

namespace WaveCare.Domain
{
    public class EmergencyContacts
    {
        public Guid Id { get; set; }
        public string ContactName { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
    }

}
