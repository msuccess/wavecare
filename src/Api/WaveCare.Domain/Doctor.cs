using System;
using System.Collections.Generic;

namespace WaveCare.Domain
{
	public class Doctor : UserBase
	{
		public string Department { get; set; }
		public string Specialization { get; set; }

		//Working Place
		public string DutyTpye { get; set; }

		public string ConsultantRoom { get; set; }
		public DateTime DateEmployed { get; set; }
		public string InTime { get; set; }
		public string OutTime { get; set; }
		public string DayOnShift { get; set; }

		//Collections
		public ICollection<Notes> Note { get; set; }

		public ICollection<Appointment> Appointments { get; set; }
	}
}
