using AutoMapper;
using IdentityAuth.Auth;
using IdentityAuth.Helpers;
using IdentityAuth.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Text;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.ApplicationCore.Messages;
using WaveCare.ApplicationCore.Services;
using WaveCare.Data;
using WaveCare.Data.Entities;
using WaveCare.Data.Interfaces;
using WaveCare.Data.Repositories;
using WaveCare.Domain;

namespace WaveCare.WebApi
{
	public class Startup
	{
		private const string SecretKey = "iNivDmHLpUA223sqsfhqGbMRdRj1PVkH";
		private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddAutoMapper();
			//Role
			services.AddAuthentication("Bearer");

			services.AddDbContext<WaveCareDbContext>(opt =>
				opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
					b => b.MigrationsAssembly("WaveCare.Data")));

			services.AddAuthentication().AddGoogle(googleOptions =>
			{
				googleOptions.ClientId = "921535133781-da9oc3s7k1s7m25mi53big3oemi37181.apps.googleusercontent.com";
				googleOptions.ClientSecret = "57C7ZVt2QVZdTwCh3HqqUqgn";
			});

			services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));

			services.AddMvc().AddJsonOptions(option =>
			{
				option.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				option.SerializerSettings.SerializationBinder = new DefaultSerializationBinder();
				option.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
				option.SerializerSettings.Formatting = Formatting.Indented;
			});

			services.AddSingleton<IJwtFactory, JwtFactory>();

			services.Configure<FacebookAuthSetting>(Configuration.GetSection(nameof(FacebookAuthSetting)));

			services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();

			var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

			services.Configure<JwtIssuerOptions>(options =>
			{
				options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
				options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
				options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
			});

			var tokenValidationParameters = new TokenValidationParameters
			{
				ValidateIssuer = true,
				ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

				ValidateAudience = true,
				ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

				ValidateIssuerSigningKey = true,
				IssuerSigningKey = _signingKey,

				RequireExpirationTime = false,
				ValidateLifetime = true,
				ClockSkew = TimeSpan.Zero
			};

			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(configureOptions =>
			{
				configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
				configureOptions.TokenValidationParameters = tokenValidationParameters;
				configureOptions.SaveToken = true;
			});

			services.AddAuthorization(options =>
			{
				options.AddPolicy("ApiUser",
					policy => policy.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol,
						Constants.Strings.JwtClaims.ApiAccess));

				options.AddPolicy("RequiredAdministrator", policy => policy.RequireRole("Administrator"));
			});

			services.AddAuthorization(options =>
			{
				options.AddPolicy("Access Resources", policy => policy.RequireRole("Administrator", "user"));
			});

			var builder = services.AddIdentity<AuthUser, IdentityRole>(opt =>
			 {
				 opt.Password.RequireDigit = false;
				 opt.Password.RequireLowercase = false;
				 opt.Password.RequireUppercase = false;
				 opt.Password.RequireNonAlphanumeric = false;
				 opt.Password.RequiredLength = 8;
				 opt.Password.RequireNonAlphanumeric = true;
			 });

			builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);

			builder.AddEntityFrameworkStores<WaveCareDbContext>().AddDefaultTokenProviders();

			services.AddCors(options => options.AddPolicy("AllowALL", p => p
				.AllowAnyOrigin()
				.AllowAnyMethod()
				.AllowAnyHeader()
				.AllowCredentials()
			));

			services.AddTransient<RoleManager<IdentityRole>>();

			//services
			services.AddTransient<IDoctorService, DoctorService>();
			services.AddTransient<IMedicationService, MedicationService>();
			services.AddTransient<IAppointmentService, AppointmentService>();
			services.AddTransient<INoteService, NoteService>();
			services.AddTransient<IPatientService, PatientService>();
			services.AddTransient<IStaffService, StaffService>();
			services.AddTransient<IMedicineService, MedicineService>();
			services.AddTransient<IEmailSender, AuthMessageSender>();

			//repositories
			services.AddTransient<IDoctorRepository, DoctorRepository>();
			services.AddTransient<IMedicationRepository, MedicationRepository>();
			services.AddTransient<IAppointmentRepository, AppointmentRepository>();
			services.AddTransient<INoteRepository, NoteRepository>();
			services.AddTransient<IPatientRepository, PatientRepository>();
			services.AddTransient<IStaffRepository, StaffRepository>();
			services.AddTransient<IMedicineRespository, MedicineRespository>();
			//uow
			services.AddTransient<IUnitOfWork, UnitOfWork>();
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseStaticFiles();

			app.UseHttpsRedirection();
			app.UseMvc();
			app.UseAuthentication();
		}
	}
}
