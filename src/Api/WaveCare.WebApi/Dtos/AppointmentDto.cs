using System;
using WaveCare.Domain;

namespace WaveCare.WebApi.Dtos
{
	public class AppointmentDto
	{
		public DateTime AppointmentDate { get; set; }
		public string TreatmentFor { get; set; }
		public string Specialization { get; set; }
		public string Status { get; set; }
		public string Details { get; set; }
		public Doctor Doctor { get; set; }
		public Patient Patient { get; set; }
	}
}
