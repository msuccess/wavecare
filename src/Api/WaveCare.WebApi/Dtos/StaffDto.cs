using System;
using System.Collections.Generic;
using WaveCare.Domain;

namespace WaveCare.WebApi.Dtos
{
	public class StaffDto
	{
		public string BloodGroup { get; set; }
		public string Department { get; set; }
		public DateTime DateEmployed { get; set; }
		public DateTime RetirementDate { get; set; }
		public ICollection<EmergencyContacts> EmergencyContacts { get; set; }
		public string EmployeeType { get; set; }
		public string Qualification { get; set; }
		public string Specialization { get; set; }
	}
}
