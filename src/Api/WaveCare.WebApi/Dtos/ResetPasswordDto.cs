using System.ComponentModel.DataAnnotations;

namespace WaveCare.WebApi.Dtos
{
	public class ResetPasswordDto
	{
		public string Id { get; set; }

		[Required]
		public string Code { get; set; }

		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; }
	}
}
