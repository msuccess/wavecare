using System.Collections.Generic;
using WaveCare.Domain;

namespace WaveCare.WebApi.Dtos
{
	public class MedicalHistoryDto
	{
		public string Description { get; set; }
		public ICollection<Prescription> Prescription { get; set; }
		public Patient Patient { get; set; }
	}
}
