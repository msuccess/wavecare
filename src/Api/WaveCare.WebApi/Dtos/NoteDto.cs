using System;

namespace WaveCare.WebApi.Dtos
{
	public class NoteDto
	{
		public string Name { get; set; }
		public string Details { get; set; }
		public Guid ParentId { get; set; }
	}
}
