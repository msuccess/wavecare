using System.Collections.Generic;
using WaveCare.Domain;

namespace WaveCare.WebApi.Dtos
{
	public class MedicationDto
	{
		public double Cost { get; set; }
		public Patient Patient { get; set; }
		public string Treatment { get; set; }
		public ICollection<Medicine> Medicine { get; set; }
	}
}
