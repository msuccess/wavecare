using System;

namespace WaveCare.WebApi.Dtos
{
	public class DoctorDto
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string UploadImage { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string Password { get; set; }
		public string LastName { get; set; }
		public string EmailAddress { get; set; }
		public string PhoneNumber { get; set; }
		public string MobileNumber { get; set; }
		public string BusinessPhone { get; set; }
		public string Occupation { get; set; }
		public string IsActive { get; set; }
		public string MaritalStatus { get; set; }
		public string Gender { get; set; }
		public DateTime DateOfBirth { get; set; }
		public string BackgroundInfo { get; set; }
		public string Specialization { get; set; }

		public string Department { get; set; }

		//Working Place
		public string DutyTpye { get; set; }

		public int ConsultantRoom { get; set; }
		public DateTime DateEmployed { get; set; }
		public string InTime { get; set; }
		public string OutTime { get; set; }
		public string DayOnShift { get; set; }

		//Address Details
		public AddressDto Address { get; set; }
	}
}
