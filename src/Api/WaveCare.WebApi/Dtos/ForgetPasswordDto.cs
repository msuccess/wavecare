using System.ComponentModel.DataAnnotations;

namespace WaveCare.WebApi.Dtos
{
	public class ForgetPasswordDto
	{
		[Required]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		public string ReturnUrl { get; set; }
	}
}
