using AutoMapper;
using WaveCare.Domain;
using WaveCare.WebApi.Dtos;

namespace WaveCare.WebApi.Mapping
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			//From ApiResource to DbModel
			CreateMap<Doctor, DoctorDto>();

			CreateMap<Address, AddressDto>();

			//From DbModel to ApiResource
			CreateMap<DoctorDto, Doctor>();

			CreateMap<AddressDto, Address>();

			//Ignored DbModel
			CreateMap<DoctorDto, Doctor>()
				.ForMember(doc => doc.Appointments, c => c.Ignore());

			CreateMap<DoctorDto, Doctor>()
				.ForMember(doc => doc.Note, c => c.Ignore());

			CreateMap<RegisterDto, AuthUser>()
				.ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));
		}
	}
}
