using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Domain;
using WaveCare.Infrastructure.Response;
using WaveCare.WebApi.Dtos;


namespace WaveCare.WebApi.Controllers
{
	[Route("api/Doctors")]
	[ApiController]
	public class DoctorsController : Controller
	{
		private readonly IDoctorService _doctorService;
		private readonly IMapper _mapper;

		public DoctorsController(IDoctorService doctorService,
			IMapper mapper)
		{
			_doctorService = doctorService;
			_mapper = mapper;
		}

		[HttpGet]
		[Route("GetDoctors")]
		public ActionResult<IEnumerable<Doctor>> Get()
		{
			var result = _doctorService.GetDoctors();

			if (result.IsSucceeded)

				return Ok(new OkListResponse(result.Message, result.Data));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpGet]
		[Route("GetDoctors/{Id}")]
		public ActionResult GetById(Guid Id)
		{
			var result = _doctorService.GetDoctorById(Id);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message, result.Data));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpGet]
		[Route("GetDoctors/{Email}")]
		public ActionResult GetByEmail(string Email)
		{
			var result = _doctorService.GetDoctorByEmail(Email);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message, result.Data));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpPost]
		[Route("AddDoctor")]
		public ActionResult AddDoctor([FromBody]DoctorDto doctorDto)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var doctor = _mapper.Map<DoctorDto, Doctor>(doctorDto);

			var result = _doctorService.AddDoctor(doctor);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpPut]
		[Route("UpdateDoctor/{Id}")]
		public ActionResult UpdateDoctor(Guid Id, [FromBody]DoctorDto updateDoctorDto)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var doctor = _mapper.Map<DoctorDto, Doctor>(updateDoctorDto);

			var result = _doctorService.UpdateDoctor(Id, doctor);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpDelete]
		[Route("DeleteDoctor/{Id}")]
		public ActionResult DeleteDoctor(Guid Id)
		{
			var result = _doctorService.DeleteDoctor(Id);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}
	}
}
