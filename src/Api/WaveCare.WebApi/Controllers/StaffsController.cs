using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Domain;
using WaveCare.Infrastructure.Response;
using WaveCare.WebApi.Dtos;

namespace WaveCare.WebApi.Controllers
{
	[Route("api/Patients")]
	[ApiController]
	public class StaffsController : Controller
	{
		private readonly IStaffService _staffService;
		private readonly IMapper _mapper;

		public StaffsController(IStaffService staffService, IMapper mapper)
		{
			_staffService = staffService;
			_mapper = mapper;
		}

		[HttpGet]
		[Route("GetStaffs")]
		public ActionResult<IEnumerable<Patient>> Get()
		{
			var result = _staffService.GetStaffs();

			if (result.IsSucceeded)

				return Ok(new OkListResponse(result.Message, result.Data));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpGet]
		[Route("GetStaff/{Id}")]
		public ActionResult GetById(Guid id)
		{
			var result = _staffService.GetStaffById(id);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message, result.Data));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		//	[HttpGet]
		//	[Route("GetPatients/{PatientNumber}")]
		//	public ActionResult GetByEmail(string PatientNumber)
		//	{
		//	    var result = _patientService;
		//
		//	    if (result.IsSucceeded)
		//
		//		    return Ok(new OkResponse(result.Message, result.Data));
		//
		//	    return BadRequest(new BadRequestResponse(result.Message));
		//	    }

		[HttpPost]
		[Route("AddStaff")]
		public ActionResult Add([FromBody]StaffDto staffDto)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var staff = _mapper.Map<StaffDto, Staff>(staffDto);

			var result = _staffService.AddStaff(staff);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpPut]
		[Route("UpdateStaff/{Id}")]
		public ActionResult Update(Guid id, [FromBody]StaffDto updateStaffDto)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var staff = _mapper.Map<StaffDto, Staff>(updateStaffDto);

			var result = _staffService.UpdateStaff(id, staff);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpDelete]
		[Route("DeleteStaff/{Id}")]
		public ActionResult DeletePatient(Guid id)
		{
			var result = _staffService.DeleteStaff(id);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}
	}
}
