using AutoMapper;
using IdentityAuth.Auth;
using IdentityAuth.Helpers;
using IdentityAuth.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Threading.Tasks;
using WaveCare.ApplicationCore.Messages;
using WaveCare.Domain;
using WaveCare.WebApi.Dtos;

namespace WaveCare.WebApi.Controllers
{
	[Route("api/Auth")]
	[ApiController]
	public class AuthController : Controller
	{
		private readonly UserManager<AuthUser> _userManager;
		private readonly IJwtFactory _jwtFactory;
		private readonly JwtIssuerOptions _jwtIssuerOptions;
		private readonly IMapper _mapper;
		private readonly IEmailSender _emailSender;

		public AuthController(
			UserManager<AuthUser> userManager,
			IJwtFactory jwtFactory,
			IOptions<JwtIssuerOptions> jwtIssuerOptions, IMapper mapper, IEmailSender emailSender)
		{
			_userManager = userManager;
			_jwtFactory = jwtFactory;
			_mapper = mapper;
			_emailSender = emailSender;
			_jwtIssuerOptions = jwtIssuerOptions.Value;
		}

		[Route("Register")]
		[HttpPost]
		public async Task<IActionResult> Register([FromBody] RegisterDto registerDto)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var userIdentity = _mapper.Map<AuthUser>(registerDto);

			var result = await _userManager.CreateAsync(userIdentity, registerDto.Password);

			if (!result.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(result, ModelState));

			await addToRole(registerDto.Username, "Administrator");

			//			await addClaims(registerDto.Username);

			var confrimationCode = await _userManager.GenerateEmailConfirmationTokenAsync(userIdentity);

			var callbackurl = Url.Action("Register", "Auth", new { userId = userIdentity.Id, code = confrimationCode, returnUrl = registerDto.ReturnUrl }, Request.Scheme);

			await _emailSender.SendEmailAsync(registerDto.Email, "Confirm your account",
				$"Please confirm your account by clicking this link:<a href='{callbackurl}'>Link</a>");

			return Ok(result);
		}

		[Route("Login")]
		[HttpPost]
		public async Task<IActionResult> Login([FromBody] CredentialsDto credential)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			var identity = await GetClaimsIdentity(credential.UserName, credential.Password);

			if (identity == null)

				return BadRequest(Errors.AddErrorToModelState("Login Fail", "Invalid Username or Password", ModelState));

			var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, credential.UserName, _jwtIssuerOptions,
				new JsonSerializerSettings { Formatting = Formatting.Indented });

			return new OkObjectResult(jwt);
		}

		[Route("ForgetPassword")]
		[HttpPost]
		public async Task<IActionResult> ForgotPassword([FromBody] ForgetPasswordDto ftPasswordDto)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var user = await _userManager.FindByNameAsync(ftPasswordDto.Email);

			if (user == null) return BadRequest();

			var code = await _userManager.GeneratePasswordResetTokenAsync(user);

			var callbackurl = Url.Action("ForgotPassword", "Auth", new { UserId = user.Id, code = code },
				Request.Scheme);
			await _emailSender.SendEmailAsync(user.Email, "Confirm your account",
				$"Please confirm your account by clicking this link:<a href='{callbackurl}'>Link</a>");

			return Ok("Email sent!");
		}

		[Route("ForgetPassword")]
		[HttpPost]
		public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordDto resetPasswordDto)
		{
			var user = await _userManager.FindByIdAsync(resetPasswordDto.Id);

			if (user == null) return NotFound("User not found");

			var result = await _userManager.ResetPasswordAsync(user, resetPasswordDto.Code.Replace('-', '/'), resetPasswordDto.Password);

			if (result.Succeeded) return Ok("Password reset successful");

			return BadRequest(result);
		}

		private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
		{
			if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
				return await Task.FromResult<ClaimsIdentity>(null);

			var userToVerify = await _userManager.FindByNameAsync(userName);

			if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

			if (await _userManager.CheckPasswordAsync(userToVerify, password))
				return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));

			return await Task.FromResult<ClaimsIdentity>(null);
		}

		//		private async Task addClaims(string userName)
		//		{
		//			var user = await _userManager.FindByNameAsync(userName);
		//			var claims = new List<Claim> {
		//				new Claim(type: JwtClaimTypes.GivenName, value: user.GivenName),
		//				new Claim(type: JwtClaimTypes.FamilyName, value: user.FamilyName),
		//			};
		//			await _userManager.AddClaimsAsync(user, claims);
		//		}

		private async Task addToRole(string userName, string roleName)
		{
			var user = await _userManager.FindByNameAsync(userName);
			await _userManager.AddToRoleAsync(user, roleName);
		}
	}
}
