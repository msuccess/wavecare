using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WaveCare.Domain;
using WaveCare.Infrastructure.Response;

namespace WaveCare.WebApi.Controllers
{
	[Authorize(Roles = "Administrator")]
	[Route("api/Role")]
	[ApiController]
	public class RoleController : Controller
	{
		private readonly UserManager<AuthUser> _userManager;
		private readonly RoleManager<IdentityRole> _roleManager;
		//private readonly SignInManager<AuthUser> _signInManager;

		public RoleController(
			UserManager<AuthUser> userManager,
			RoleManager<IdentityRole> roleManager,
			SignInManager<AuthUser> signInManager)
		{
			_userManager = userManager;
			_roleManager = roleManager;
			//_signInManager = signInManager;
		}

		//Get User Roles
		[HttpGet("GetUsersByRole{roleName}")]
		public async Task<IActionResult> GetUsersByRole(string roleName)
		{
			//var role = await _roleManager.FindByNameAsync(roleName);

			var user = await _userManager.GetUsersInRoleAsync(roleName);

			return Ok(new OkListResponse(user));
		}

		//Delete Role
		[HttpDelete("DeleteRole")]
		public async Task<IActionResult> DeleteRole(string userName, string roleName)
		{
			var user = await _userManager.FindByNameAsync(userName);

			if (user == null) return NotFound("User not found");

			var result = await _userManager.RemoveFromRoleAsync(user, roleName);

			if (!result.Succeeded) return BadRequest(result.Errors);

			return Ok();
		}

		//Add Role
		[HttpPost("AssignRole")]
		public async Task<IActionResult> AssignRoleToUser([FromBody]UserRole userRole)
		{
			var user = await _userManager.FindByNameAsync(userRole.username);

			if (user == null) return NotFound("User not found");

			await _userManager.AddToRoleAsync(user, userRole.roleName);

			return Ok("User Assign Role Successfully");
		}

		[HttpGet("CreateRole/{roleName}")]
		public async Task<IActionResult> CreateRole(string roleName)
		{
			var role = await _roleManager.RoleExistsAsync(roleName);

			if (role) return BadRequest("Role Already Exist");

			var identityRole = new IdentityRole { Name = roleName };

			var result = await _roleManager.CreateAsync(identityRole);

			if (!result.Succeeded) return BadRequest(result);

			return Ok("Role Added");
		}

		[HttpGet("GetRoles")]
		public ActionResult GetRoles()
		{
			var roles = _roleManager.Roles;
			return Ok(new OkResponse(roles));
		}
	}

	public class UserRole
	{
		public string roleName { get; set; }
		public string username { get; set; }
	}
}
