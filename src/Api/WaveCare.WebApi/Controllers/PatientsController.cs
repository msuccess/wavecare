using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WaveCare.ApplicationCore.Interfaces;
using WaveCare.Domain;
using WaveCare.Infrastructure.Response;
using WaveCare.WebApi.Dtos;

namespace WaveCare.WebApi.Controllers
{
	[Route("api/Patients")]
	[ApiController]
	public class PatientsController : Controller
	{
		private readonly IPatientService _patientService;
		private readonly IMapper _mapper;

		public PatientsController(IPatientService patientService, IMapper mapper)
		{
			_patientService = patientService;
			_mapper = mapper;
		}

		[HttpGet]
		[Route("GetPatients")]
		public ActionResult<IEnumerable<Patient>> Get()
		{
			var result = _patientService.GetPatients();

			if (result.IsSucceeded)

				return Ok(new OkListResponse(result.Message, result.Data));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpGet]
		[Route("GetPatients/{Id}")]
		public ActionResult GetById(Guid id)
		{
			var result = _patientService.GetPatientById(id);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message, result.Data));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpPost]
		[Route("AddPatient")]
		public ActionResult AddPatient([FromBody]PatientDto patientDto)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var patient = _mapper.Map<PatientDto, Patient>(patientDto);

			var result = _patientService.AddPatient(patient);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpPut]
		[Route("UpdatePatient/{Id}")]
		public ActionResult UpdatePatient(Guid id, [FromBody]PatientDto updatePatient)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			var patient = _mapper.Map<PatientDto, Patient>(updatePatient);

			var result = _patientService.UpdatePatient(id, patient);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}

		[HttpDelete]
		[Route("DeletePatient/{Id}")]
		public ActionResult DeletePatient(Guid id)
		{
			var result = _patientService.DeletePatient(id);

			if (result.IsSucceeded)

				return Ok(new OkResponse(result.Message));

			return BadRequest(new BadRequestResponse(result.Message));
		}
	}
}
